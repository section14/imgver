package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	deployPtr := flag.String("f", "", "Deployment file to read in")
	imagePtr := flag.String("i", "", "Image name with tag ex: someimage:v1.0.0")
	flag.Parse()

	deploy := *deployPtr
	image := *imagePtr

	if deploy == "" {
		log.Fatal("-f must be point to deployment file")
	}

	if image == "" {
		log.Fatal("-i must contain name of versioned image")
	}

	err := updateDeployment(deploy, image)
	if err != nil {
		log.Println(err)
		return
	}

	log.Printf("deployment updated with image: %s\n", image)
}

func updateDeployment(deploy, imageName string) error {
	//open deployment file
	f, err := os.OpenFile(deploy, os.O_APPEND|os.O_RDWR, os.ModeAppend)
	if err != nil {
		return err
	}
	defer f.Close()

	tempName := "deploy.tmp"

	//create tmp file to write to
	temp, err := os.Create(tempName)
	if err != nil {
		return err
	}
	defer temp.Close()

	//make sure we did something
	replaced := false

	//iterate until we reach the image line
	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		//replace line with new image name
		if strings.Contains(scanner.Text(), "image: ") {
			split := strings.SplitAfter(scanner.Text(), "image: ")
			line := fmt.Sprintf("%s%s\n", split[0], imageName)
			temp.WriteString(line)
			replaced = true
			continue
		}

		//write existing data
		temp.WriteString(scanner.Text() + "\n")
	}

	//nothing happened, delete temp
	if replaced == false {
		temp.Close()

		err := deleteTemp(tempName)
		log.Println(err)

		return errors.New("There wasn't an \"image: \" entry in your deployment file")
	}

	temp.Close()
	f.Close()

	//copy from temp to original
	err = copyData(deploy, tempName)
	if err != nil {
		return err
	}

	err = deleteTemp(tempName)
	if err != nil {
		return err
	}

	return nil
}

func copyData(deploy, tempName string) error {
	f, err := os.OpenFile(deploy, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	f.Truncate(0)
	if err != nil {
		return err
	}
	defer f.Close()

	temp, err := os.OpenFile(tempName, os.O_APPEND|os.O_RDONLY, os.ModeAppend)
	if err != nil {
		return err
	}
	defer temp.Close()

	//replace our deployment file with the temp, and delete the temp
	amt, err := io.Copy(f, temp)
	if err != nil {
		return err
	}

	if amt == 0 {
		return errors.New("0 bytes were copied from temp")
	}

	temp.Close()
	f.Close()

	return nil
}

func deleteTemp(tempName string) error {
	err := os.Remove(tempName)
	if err != nil {
		return err
	}

	return nil
}
